# React Jobs List Project

In this project, the list of jobs was sorted using the Github Jobs Api.

## Content

The repository contains:

- a [React.js](https://facebook.github.io/react-native/)
- a [Webpack](https://webpack.js.org/)
- a [Sass](https://sass-lang.com/)
- a [clear directory layout](#directory-layout) to provide a base architecture
- for CORS problem : https://chrome.google.com/webstore/detail/cors-unblock/lfhmikememgdcahcdlaciloancbhjino/

## Directory layout

- [`public/`](public): html files and favicon 
- [`src/components`](src/components): presentational components
- [`src/api`](src/api): the api service
- [`src/styles`](src/styles): project styles

## Prerequisites
```
Download and install npm with Node.js @ https://nodejs.org/en
```

## Installing
Select a folder, navigate to it, and clone this repository with this command-line:
```
git clone https://github.com/oguzsh/job-list-project.git
```

Install the dependencies with this command-line:
```
npm install or yarn install
```

## Build and Running
Run: 
```
yarn start / npm start
```
Build: 
```
yarn build / npm build
```


## License

This project is released under the [MIT License](LICENSE).
