const URI = "https://jobs.github.com/positions.json";

export const getAllJobs = () => {
  return fetch(URI).then(response => response.json());
};

export const getDescriptionJobs = description => {
  return fetch(`${URI}?description=${description}`);
};

export const getLocationJobs = location => {
  return fetch(`${URI}?location=${location}`);
};

export const getDescriptionWithLocation = (description, location) => {
  return fetch(
      `${URI}?description=${description}&location=${location}`
  );
};
