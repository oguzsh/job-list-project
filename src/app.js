import React, { useEffect, useState } from "react";
import styles from "./styles/main.sass";

// Service
import { getAllJobs } from "./api/githubService";

// Components
import Layout from "./components/Layout";
import CardList from "./components/CardList";
import Loading from "./components/Loading";

const App = () => {
  const [jobs, setJobs] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      const response = await getAllJobs();
      setJobs(response);
      setLoading(false);
    }
    fetchData();
  }, []);

  return (
    <>
      <Layout>
        {(loading) ? <Loading/> : <CardList data={jobs}/>}
      </Layout>
    </>
  );
};

export default App;
