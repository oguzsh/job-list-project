import React from 'react';

import styles from './card-style.sass'

const Card  = ({data}) => {
    return(
        <>
            <div className={styles.card}>
                <div className={styles.cardImage}><img src={data.company_logo || "https://github.githubassets.com/images/modules/logos_page/GitHub-Logo.png"} width="180"  alt="Company Logo"/></div>
                <div className={styles.cardContent}>
                    <h2 className={styles.cardTitle}>{data.title}</h2>
                    <span className={styles.cardCompany}>{data.company}</span>
                    <p className={styles.cardStatus}>{data.type}</p>
                    <a href={data.url} target="_blank" rel="noreferrer" className={styles.btn}>Apply job</a>
                </div>
            </div>
        </>
    )
};

export default Card;
