import React from 'react';

import styles from './card-list-style.sass'
import Card from "../Card";

const CardList  = ({data}) => {
    return(
        <>
            <h1>All Jobs</h1>
            <ul className={styles.cards}>
                {data.map(item => <Card key={item.id} data={item}/>)}
            </ul>
        </>
    )
};

export default CardList;
