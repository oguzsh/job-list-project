import React from 'react';

import styles from './footer-style.sass'

// Icons
import { FaTwitter, FaGithub, FaLinkedinIn, FaCodepen } from 'react-icons/fa';

const Footer = () => {
    return(
        <footer>
            <div className={styles.leftSide}>
                <a href="https://oguz.live" target="_blank" rel="noreferrer">© 2020 oguzsh</a>
            </div>
            <div className={styles.middle}>
                <img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Logo.png" width={100} alt="Logo"/>
            </div>
            <div className={styles.rightSide}>
                <ul>
                    <li><a href="https://twitter.com/oguz_sh" target="_blank" rel="noreferrer"><FaTwitter/></a></li>
                    <li><a href="https://github.com/oguzsh" target="_blank" rel="noreferrer"><FaGithub/></a></li>
                    <li><a href="https://www.linkedin.com/in/oguzhanince/" target="_blank" rel="noreferrer"><FaLinkedinIn/></a></li>
                    <li><a href="https://codepen.io/0guzhan" target="_blank" rel="noreferrer"><FaCodepen/></a></li>
                </ul>
            </div>
        </footer>
    )
};

export default Footer;
