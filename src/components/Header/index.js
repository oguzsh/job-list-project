import React from 'react';

import styles from './header-style.sass';

const Header = () => {
    return(
        <header className={styles.mainHeader}>
            <div className={styles.container}>
                <a href="https://github.com/oguzsh" target="_blank" rel="noreferrer" className={styles.logo}>
                    <img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Logo.png" width="150" height="75" alt="Github Logo" />
                </a>

                <nav className={styles.mainNav}>
                    <ul>
                        <li>
                            <a href="/" rel="noreferrer">Home</a>
                        </li>
                        <li>
                            <a href="https://oguz.live" target="_blank" rel="noreferrer">About Me</a>
                        </li>
                        <li>
                            <a href="mailto:oguzhan824@gmail.com">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    )
};

export default Header;
