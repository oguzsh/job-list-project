import React from 'react';

// Components
import Header from "../Header"
import Footer from "../Footer"

// Styles
import styles from './layout.sass'

 const Layout  = ({children}) => {
   return(
       <div className={styles.mainContainer}>
           <Header/>
           <main className={styles.content}>{children}</main>
           <Footer/>
       </div>
   )
};

export default Layout;
