import React from 'react';

import styles from './loading-style.sass'

const Loading = () => {
    return(
       <>
           <div className={styles.loader}>
               <div></div>
               <div></div>
               <div></div>
               <div></div>
               <div></div>
               <div></div>
               <div></div>
               <div></div>
           </div>
           <p>Loading...</p>
       </>
    )
};

export default Loading;
